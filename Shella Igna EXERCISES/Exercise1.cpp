// ConsoleApplication46.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

int Factorial(int x) {

	if (x > 1)
		return x * Factorial(x - 1);
	else
		return 1;
}

int main()
{
	int x;
	cout << "Enter a number: ";
	cin >> x;
	cout << "Factorial of " << x << " ! " << "= " << Factorial(x);
	return 0;
}

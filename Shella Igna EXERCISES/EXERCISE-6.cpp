// ConsoleApplication46.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

void sortingArray(int array[], int n) {
	int i, j, min, temp;
	for (i = 0; i < n - 1; i++) {
		min = i;
		for (j = i + 1; j < n; j++)
			if (array[j] < array[min])
				min = j;
		temp = array[i];
		array[i] = array[min];
		array[min] = temp;
	}
}

int main() {
	int array[10] = { 26, 95, 36, 75, 10, 8, 72, 99, 1, 67 };
	int n = sizeof(array) / sizeof(array[0]);
	int i;

	cout << "Given array is:" << endl;
	for (i = 0; i < n; i++)
		cout << array[i] << " ";
	cout << endl;

	sortingArray(array, n);
	cout << "Sorted array is: " << endl;

	for (i = 0; i < n; i++)
		cout << array[i] << " ";
	return 0;
}
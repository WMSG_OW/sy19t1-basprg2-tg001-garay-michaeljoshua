// ConsoleApplication46.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

void printingArray(string array[], int sizeofArray) {
	for (int i = 0; i < sizeofArray; i++) {
		cout << array[i] << endl;
	}
}

int main() {
	string items[8] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };

	printingArray(items, 8);

	return 0;
}

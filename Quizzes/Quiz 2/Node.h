#ifndef BASPROG2_NODE_H
#define BASPROG_NODE_H

#include <string>

using namespace std;

struct Node
{
	string soldierName;
	Node* next;
};
#endif // !BASPROG2_NODE_H
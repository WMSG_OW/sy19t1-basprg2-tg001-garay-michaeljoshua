#include<iostream>
#include<string>
#include <time.h>
#include "Node.h"

using namespace std;

// LordCommander picking his first player to hold da cloak
Node *firstPick(Node *head, Node *temp, int &numofSoldiers)
{
	int lordCommander = rand() % numofSoldiers + 1;

	for (int i = 0; i < numofSoldiers; i++)
	{
		if (i == lordCommander)
		{
			head = temp;
		}
		temp = temp->next;
	}
	cout << " The Lord Commander has chosen " << head->soldierName << " to be the first bearer of the cloak" << endl;
	system("pause");
	return head;
}

// creation of linked list and its data
Node *createList(Node *head, Node *temp, int &numofSoldiers)
{
	srand(time(NULL));
	for (int i = 0; i < numofSoldiers; i++)
	{
		Node *next = new Node;

		if (head == NULL)
		{
			head = next;
			temp = next;
			cout << "What is your Soldiers Name? ";
			cin >> temp->soldierName;
			next->next = head;
		}
		else
		{
			temp->next = next;
			temp = next;
			cout << "What is your Soldiers Name? ";
			cin >> temp->soldierName;
			temp->next = head;
		}
		system("cls");
	}
	
	head = firstPick(head, temp, numofSoldiers);

	return head;
}

//delete members based on random draw (will count from head)
Node *memberDelete(Node *head, int &numofSoldiers)
{
	Node *currentNode = head;
	Node *temp = head;
	int randomDraw = rand() % numofSoldiers + 1;

	cout << currentNode->soldierName << " has drawn " << randomDraw << endl;

	for (int i = 0; i <= numofSoldiers; i++)
	{
		if (i == randomDraw)
		{
			cout << currentNode->soldierName << " has been eliminated " << endl;

			while (temp->next != currentNode)
			{
				temp = temp->next;
			}

			head = currentNode->next;

			temp->next = head;

			delete currentNode;

	        return head;
		}
		currentNode = currentNode->next;
	}

}

// display soldiers
void printList(Node *head, int &numofSoldiers)
{
	Node *currentNode = head;
	
	for (int i = 0; i < numofSoldiers; i++)
	{
		cout << currentNode->soldierName << endl;
		currentNode = currentNode->next;
	}
}

int main()
{
	srand(time(NULL));

	int counter = 1;
	int numofSoldiers;
	Node *head = NULL;
	Node *temp = NULL;

	cout << "How many Soldiers do you want to have??? " << endl;
	cin >> numofSoldiers;

	head = createList(head, temp, numofSoldiers);
	do
	{
		cout << "===== ROUND " << counter << " ===== " << endl;

		printList(head, numofSoldiers);
		head = memberDelete(head, numofSoldiers);
		
		cout << "===== RESULTS ===== " << endl;
		cout << "REMAINING MEMBERS :" << endl;
		numofSoldiers--;

		printList(head, numofSoldiers);

		counter++;
		system("pause");
		system("cls");

	} while (numofSoldiers > 1);
	
	if (numofSoldiers == 1)
	{
		cout << "=================" << endl;
		cout << "  FINAL RESULTS " << endl;
		cout << "=================" << endl;
		cout << head->soldierName << " will seek out reinforcements " << endl;
	}

	system("pause");
	return 0;
}

/*List of sources I used to help me comprehend lol this is so sad tbh
https://www.codesdope.com/blog/article/c-linked-lists-in-c-singly-linked-list/
https://www.codementor.io/codementorteam/a-comprehensive-guide-to-implementation-of-singly-linked-list-using-c_plus_plus-ondlm5azr
https://www.codeproject.com/articles/24684/how-to-create-linked-list-using-c-c
https://www.geeksforgeeks.org/c-programs-gq/linked-list-programs-gq/
https://www.tutorialspoint.com/cplusplus-program-to-implement-circular-singly-linked-list
https://www.geeksforgeeks.org/circular-singly-linked-list-insertion/
https://www.thecrazyprogrammer.com/2015/12/circular-linked-list-in-c.html
   https://www.bing.com/videos/search?q=singly+linked+lists+c%2b%2b&&view=detail&mid=5D44E3F8503F363A0D835D44E3F8503F363A0D83&&FORM=VRDGAR
   https://www.bing.com/videos/search?q=singly+linked+lists+c%2b%2b&&view=detail&mid=8CBBC60B6F870086255A8CBBC60B6F870086255A&&FORM=VRDGAR
   https://youtu.be/hwRFWYz4Frg

*/
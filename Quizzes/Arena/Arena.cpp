#include<iostream>
#include<time.h>
#include<string>
#include"Character.h"

using namespace std;

//display character stats
void displayStats(Character *Player)
{
	Player->viewStats();
}

int main()
{
	srand(time(NULL));

	int stage = 1;
	char choice;

	//Start sequence
	Character *Player = new Character();
	Player->createPlayer(Player);
	displayStats(Player);

	Character *Enemy = new Character();
	Enemy->chooseRandomEnemy(Enemy);
	displayStats(Enemy);

	cout << "The battle will begin..." << endl;
	system("pause");
	system("cls");

	   while (Player->getHp() > 0)
		{
			system("pause");

			cout << Player->getName() << " : " << Player->getHp() << endl;
			cout << Enemy->getName() << Enemy->getType() << " : " << Enemy->getHp() << endl;

			Player->applyDamage(Player, Enemy);

			if (Player->getHp() > 0 && Enemy->getHp() <= 0)
			{
				stage++;
				Player->setHp(Player->getHp());
				if (Player->getType() == "Assassin")
				{
					Player->setAgi(Player->getAgi());
					Player->setDex(Player->getDex());
				}
				else if (Player->getType() == "Assassin")
				{
					Player->setAgi(Player->getAgi());
					Player->setDex(Player->getDex());
				}
				else if (Player->getType() == "Warrior")
				{
					Player->setVit(Player->getVit());
				}
				else if (Player->getType() == "Mage")
				{
					Player->SetPow(Player->getPow());
				}
				Enemy->chooseRandomEnemy(Enemy);
				displayStats(Enemy);
			}
			if (Player->getHp() <= 0 && Enemy->getHp() > 0)
			{
				cout << "You lost at stage " << stage << endl;
				system("pause");
				break;
			}
	   }
	
	delete Player;
	delete Enemy;
	system("pause");
	return 0;
}
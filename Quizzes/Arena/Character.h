#pragma once
#include<string>
#include<iostream>
#include<vector>

using namespace std;

class Enemies;
class Types;
class Character

{
public:
	Character();
	Character(string mName, string mType, int mHealth, int mDamage, int mPower, int mVitality, int mAgility, int mDexterity);
	~Character();

	//setters
	void setName(string name);

	void setType(string type);

	void setHp(int hp);

	void setVit(int vit);

	void setAgi(int agi);

	void SetPow(int pow);

	void setDex(int dex);
	
	void setDamage(int damage);

	//getters
	string getName();

	string getType();

	int getHp();

	int getDex();

	int getVit();

	int getPow();

	int getAgi();

	int getDamage();

	//stat viewing
	void viewStats();
    
	//other functions

	Character *Player;

	void createPlayer(Character *Player);

	Character *Enemy;

	void chooseRandomEnemy(Character *Enemy);

	void applyDamage(Character *Player, Character *Enemy);

private:
	string mName;
	string mType;
	int mHealth;
	int mDamage;
	int mPower;
	int mVitality;
	int mAgility;
	int mDexterity;
};
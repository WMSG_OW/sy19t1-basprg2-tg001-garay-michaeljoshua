#include "Character.h"
#include <time.h>
 
Character::Character()
{
	mName = "";
	mType = "";
	mHealth = 100;
	mDamage;
	mPower = rand() % 300 + 20;
	mAgility = rand() % 200 + 20;
	mDexterity = rand() % 200 + 20;
	mVitality = rand() % 200 + 20;
}

Character::Character(string mName, string mType, int mHealth, int mDamage, int mPower, int mVitality, int mAgility, int mDexterity)
{
	mName = mName;
	mType = mType;
	mHealth = mHealth;
	mDamage = mDamage;
	mAgility = mAgility;
	mDexterity = mDexterity;
	mVitality = mVitality;
}

Character::~Character()
{
	cout << "Character:" << this << "deleted" << endl;
}

void Character::setName(string name)
{
	mName = name;
}

void Character::setType(string type)
{
	mType = type;
}

void Character::setHp(int hp)
{
	mHealth = hp + (hp *0.30);

	if(Player->getType() == "Warrior")
	mHealth = hp + (hp *0.30) + 3;
}

void Character::setVit(int vit)
{
	mVitality = vit + 3;
}

void Character::setAgi(int agi)
{
	mAgility = agi + 3;
}

void Character::SetPow(int pow)
{
	mPower = pow + 5;
}

void Character::setDex(int dex)
{
	mDexterity = dex + 3;
}

void Character::setDamage(int damage)
{
	mDamage = damage;
}

string Character::getName()
{
	return mName;
}

string Character::getType()
{
	return mType;
}

int Character::getHp()
{
	return mHealth;
}

int Character::getDex()
{
	return mDexterity;
}

int Character::getVit()
{
	return mVitality;
}

int Character::getPow()
{
	return mPower;
}

int Character::getAgi()
{
	return mAgility;
}

int Character::getDamage()
{
	return mDamage;
}

void Character::viewStats()
{
	cout << "NAME : " << getName() << endl;
	cout << "CLASS TYPE: " << getType() << endl;
	cout << "HEALTH : " << getHp() << endl;
	cout << "DAMAGE : " << getDamage() << endl;
	cout << "POWER : " << getPow() << endl;
	cout << "VITALITY : " << getVit() << endl;
	cout << "AGILITY : " << getAgi() << endl;
	cout << "DEXTERITY :" << getDex() << endl;
}

void Character::createPlayer(Character * Player)
{
	string name;
	int choice;
	string Type[3]{ " Assassin", " Warrior", "Mage" };
	cout << "Please Input Player name " << endl;
	cin >> name;
	Player->setName(name);
	system("pause");
	system("cls");
	cout << "Choose a class..." << endl;
	cout << R"(   [0]   Assassin    [1]  Warrior     [2]  Mage)" << endl;
	cin >> choice;
	if (choice == 0)
	{
		Player->setType(Type[choice]);
	}
	else if (choice == 1)
	{
		Player->setType(Type[choice]);
	}
	else if (choice == 2)
	{
		Player->setType(Type[choice]);
	}
	system("pause");
	system("cls");
}

void Character:: chooseRandomEnemy(Character *Enemy)
{
	srand(time(NULL));
	int randomEnemy = rand() % 3 + 1;

	if (randomEnemy == 1)
	{
		this->Enemy = new Character("Enemy", "Assassin", mHealth, mDamage, mPower, mVitality, mAgility, mDexterity);
		Enemy->setName("Enemy");
		Enemy->setType("Assassin");
	}
	else if (randomEnemy == 2)
	{
		this->Enemy = new Character("Enemy", "Warrior", mHealth, mDamage, mPower, mVitality, mAgility, mDexterity);
		Enemy->setName("Enemy");
		Enemy->setType("Warrior");
	}
	else if (randomEnemy == 3)
	{
		this->Enemy = new Character("Enemy", "Mage", mHealth, mDamage, mPower, mVitality, mAgility, mDexterity);
		Enemy->setName("Enemy");
		Enemy->setType("Mage");
	}
}

void Character::applyDamage(Character *Player, Character *Enemy)
{
	srand(time(NULL));

	int hit;
	
		if (Player->getType() == "Assassin" && Enemy->getType() == "Warrior")
		{
			cout << "Enemy advantage";
			hit = (Player->getDex() / Enemy->getAgi()) * 100;	
			Player->setDamage((Player->getPow() / Enemy->getVit()));
			if (hit > 45 || hit > 80)
			{
				cout << "Player Attack " << endl;
				Enemy->mHealth -= Player->getDamage();
				hit = 80;
			}
			else
			{
				cout << "attack missed" << endl;
				hit = 20;
			}
			hit = (Enemy->getDex() / Player->getAgi()) * 100;
			Enemy->setDamage((Enemy->getPow() / Player->getVit()) * 1.5);
			if (hit > 45 || hit > 80)
			{
				cout << "Enemy Attack " << endl;
              Player->mHealth -= Enemy->getDamage();
			  hit = 80;
			}
			else
			{
				cout << "attack missed" << endl;
				hit = 20;
			}
			system("pause");
			system("cls");
		}
		else if (Player->getType() == "Assassin" && Enemy->getType() == "Mage")
		{
			cout << "Player advantage";
			hit = (Player->getDex() / Enemy->getAgi()) * 100;
			Player->setDamage((Player->getPow() / Enemy->getVit()) * 1.5);
			if (hit > 45 || hit > 80)
			{
				cout << "Player Attack " << endl;
				Enemy->mHealth -= Player->getDamage();
				hit = 80;
			}
			else
			{
				cout << "attack missed" << endl;
				hit = 20;
			}
			hit = (Enemy->getDex() / Player->getAgi()) * 100;
			Enemy->setDamage((Enemy->getPow() / Player->getVit()));
			if (hit > 45 || hit > 80)
			{
				cout << "Enemy Attack " << endl;
				Player->mHealth -= Enemy->getDamage();
				hit = 80;
			}
			else
			{
				cout << "attack missed" << endl;
				hit = 20;
			}
			system("pause");
			system("cls");
		}
		else if (Player->getType() == "Warrior" && Enemy->getType() == "Assassin")
		{
			cout << "Player advantage" << endl;
			hit = (Player->getDex() / Enemy->getAgi()) * 100;
			Player->setDamage((Player->getPow() / Enemy->getVit()) * 1.5);
			if (hit > 45 || hit > 80)
			{
				cout << "Player Attack " << endl;
				Enemy->mHealth -= Player->getDamage();
				hit = 80;
			}
			else
			{
				cout << "attack missed" << endl;
				hit = 20;
			}
			hit = (Enemy->getDex() / Player->getAgi()) * 100;
			Enemy->setDamage((Enemy->getPow() / Player->getVit()));
			if (hit > 45 || hit > 80)
			{
				cout << "Enemy Attack " << endl;
				Player->mHealth -= Enemy->getDamage();
				hit = 80;
			}
			else
			{
				cout << "attack missed" << endl;
				hit = 20;
			}
			system("pause");
			system("cls"); cout << "Player Attack " << endl;
			Player->setDamage((Player->getPow() / Enemy->getVit())* 1.5);
			cout << "Enemy Attack " << endl;
			Enemy->setDamage((Enemy->getPow() / Player->getVit()));

			Player->mHealth -= Enemy->getDamage();
			Enemy->mHealth -= Player->getDamage();
			system("pause");
			system("cls");
		}
		else if (Player->getType() == "Warrior" && Enemy->getType() == "Mage")
		{
		cout << "Enemy advantage";
		hit = (Player->getDex() / Enemy->getAgi()) * 100;
		Player->setDamage((Player->getPow() / Enemy->getVit()));
		if (hit > 45 || hit > 80)
		{
			cout << "Player Attack " << endl;
			Enemy->mHealth -= Player->getDamage();
			hit = 80;
		}
		else
		{
			cout << "attack missed" << endl;
			hit = 20;
		}
		hit = (Enemy->getDex() / Player->getAgi()) * 100;
		Enemy->setDamage((Enemy->getPow() / Player->getVit()) * 1.5);
		if (hit > 45 || hit > 80)
		{
			cout << "Enemy Attack " << endl;
			Player->mHealth -= Enemy->getDamage();
			hit = 80;
		}
		else
		{
			cout << "attack missed" << endl;
			hit = 20;
		}
		system("pause");
		system("cls");
		}
		else if (Player->getType() == "Mage" && Enemy->getType() == "Assassin")
		{
		cout << "Player advantage";
		hit = (Player->getDex() / Enemy->getAgi()) * 100;
		Player->setDamage((Player->getPow() / Enemy->getVit()) * 1.5);
		if (hit > 45 || hit > 80)
		{
			cout << "Player Attack " << endl;
			Enemy->mHealth -= Player->getDamage();
			hit = 80;
		}
		else
		{
			cout << "attack missed" << endl;
			hit = 20;
		}
		hit = (Enemy->getDex() / Player->getAgi()) * 100;
		Enemy->setDamage((Enemy->getPow() / Player->getVit()));
		if (hit > 45 || hit > 80)
		{
			cout << "Enemy Attack " << endl;
			Player->mHealth -= Enemy->getDamage();
			hit = 80;
		}
		else
		{
			cout << "attack missed" << endl;
			hit = 20;
		}
		system("pause");
		system("cls");
		}
		else if (Player->getType() == "Mage" && Enemy->getType() == "Warrior")
		{
		cout << "Enemy advantage";
		hit = (Player->getDex() / Enemy->getAgi()) * 100;
		Player->setDamage((Player->getPow() / Enemy->getVit()));
		if (hit > 45 || hit > 80)
		{
			cout << "Player Attack " << endl;
			Enemy->mHealth -= Player->getDamage();
			hit = 80;
		}
		else
		{
			cout << "attack missed" << endl;
			hit = 20;
		}
		hit = (Enemy->getDex() / Player->getAgi()) * 100;
		Enemy->setDamage((Enemy->getPow() / Player->getVit()) * 1.5);
		if (hit > 45 || hit > 80)
		{
			cout << "Enemy Attack " << endl;
			Player->mHealth -= Enemy->getDamage();
			hit = 80;
		}
		else
		{
			cout << "attack missed" << endl;
			hit = 20;
		}
		system("pause");
		system("cls");
		}
		else
		{
			cout << "Fair match" << endl;
			hit = (Player->getDex() / Enemy->getAgi()) * 100;
			Player->setDamage((Player->getPow() / Enemy->getVit()));
			if (hit > 45 || hit > 80)
			{
				cout << "Player Attack " << endl;
				Player->mHealth -= Enemy->getDamage();
				hit = 80;
			}
			else
			{
				cout << "attack missed" << endl;
				hit = 20;
			}
			hit = (Enemy->getDex() / Player->getAgi()) * 100;
			Enemy->setDamage((Enemy->getPow() / Player->getVit()));
			if (hit > 45 || hit > 80)
			{
				cout << "Enemy Attack " << endl;
				Player->mHealth -= Enemy->getDamage();
				hit = 80;
			}
			else
			{
				cout << "attack missed" << endl;
				hit = 20;
			}
			system("pause");
			system("cls");
		}

}


#include<iostream>
#include<time.h>
#include<vector>
#include<string>

using namespace std;

//flavour text 
void splashScreen()
{
	cout << R"( .------..------..------..------..------..------.
 |E.--. ||-.--. ||C.--. ||A.--. ||R.--. ||D.--. |
 | (\/) || (\/) || :/\: || (\/) || :(): || :/\: |
 | :\/: || :\/: || :\/: || :\/: || ()() || (__) |
 | '--'E|| '--'-|| '--'C|| '--'A|| '--'R|| '--'D|
 `------'`------'`------'`------'`------'`------)" << endl;

	cout << "By Michael Joshua A. Garay TG001 " << endl;

	cout << "A Mid term Exam project based on the game Emperor Card from the Anime Gyakkyou Burai Kaiji " << endl;

	system("pause");
	system("cls");
}

// printing the playercards every round depending on what round 
void printCards(vector<string>&cards)
{
		cout << " The cards in your current hand are " << endl;
	for (unsigned int i = 0; i < cards.size(); i++)
	{
		cout << " [ " << i << " ]" << cards[i] << endl;
	}

}

//delete card set depending on round
void deleteCard(vector<string>&cards, vector<string>&cards2, int choice, int round)
{
	if (round > 0 && round < 4 || round > 7 && round < 10)
	{
       cards.erase(cards.begin() + choice);
	}
	else if (round > 3 && round < 7 || round > 9 && round < 13)
	{
		cards2.erase(cards2.begin() + choice);
	}
		
}

//gold addition
int goldAddition(int& gold, int reward)
{
	gold += reward;

	return gold;
}

//subtracting milimeters
int subtractMeasure(int& measurement, int bet)
{
	measurement = measurement - bet;
	return measurement;
}

//conditions for winning round and adding gold or reducing milimeter count
int conditionals(int playerChoice, int aiRoll, int bet, int& measurement, int& gold, int round, vector<string>&cards, vector<string>&cards2)
{

	int prize = 100000;
	if (round > 0 && round < 4 || round > 7 && round < 10)
	{
		if (playerChoice != cards.size() && aiRoll != cards2.size())
		{
			cout << " Draw " << endl;
		}
		else if (playerChoice != cards.size() && aiRoll == cards2.size())
		{
			prize = prize * bet;
			cout << " Kaiji has won the round" << endl;
			goldAddition(gold, prize);
			return gold;
		}
		else if (playerChoice == cards.size() && aiRoll != cards2.size())
		{
			prize = prize * bet;
			cout << " Kaiji has won the round " << endl;
			goldAddition(gold, prize);
			return gold;
		}
		else if (playerChoice == cards.size() && aiRoll == cards2.size())
		{
			cout << " Kaiji has lost the round " << endl;
			subtractMeasure(measurement, bet);
			return measurement;
		}
	}
	else if (round > 3 && round < 7 || round > 9 && round < 13)
	{
		if (playerChoice != cards2.size() && aiRoll != cards.size())
		{
			cout << " Draw " << endl;
		}
		else if (playerChoice != cards2.size() && aiRoll == cards.size())
		{
			cout << "Nothing happens " << endl;
		}
		else if (playerChoice == cards2.size() && aiRoll != cards.size())
		{
			cout << " Kaiji has lost the round " << endl;
			subtractMeasure(measurement, bet);
			return measurement;
		}
		else if (playerChoice == cards.size() && aiRoll == cards2.size())
		{
			cout << " Kaiji has won the round " << endl;
			prize = prize * (bet + 5);
			goldAddition(gold, prize);
		}
	}
}

//play round (whats executed every single round)
int playRound(int& gold, int& round, int& measurement, vector<string>&cards, vector<string>&cards2)
{

	int random = 5;
	int aiChoice = rand() % random;
	int bet;


	cout << " Gold " << gold << endl;
	cout << " Milimeters left " << measurement << endl;

	cout << "Enter your bet " << endl;
	cin >> bet;

	while (bet <= 0 || bet > measurement)
	{
		cout << "Enter your bet " << endl;
		cin >> bet;
	}

	int cardPick = -1;
	
	for (;;)
	{
		if (round > 0 && round < 4 || round > 7 && round < 10)
		{
			printCards(cards);
			cout << "Pick a card.... " << endl;
			cin >> cardPick;

			system("pause");
			system("cls");

			cout << "You have chosen : " << cards[cardPick] << endl;

			system("pause");

			cout << " Your Opponent has chosen : " << cards2[aiChoice] << endl;

			system("pause");
			system("cls");
			deleteCard(cards, cards2, cardPick, round);
		}
		else if (round > 3 && round < 7 || round > 9 && round < 13)
		{

			printCards(cards2);
			cout << "Pick a card.... " << endl;
			cin >> cardPick;

			system("pause");
			system("cls");

			cout << "You have chosen : " << cards2[cardPick] << endl;

			system("pause");

			cout << " Your Opponent has chosen : " << cards[aiChoice] << endl;

			system("pause");
			system("cls");
			deleteCard(cards, cards2, cardPick, round);
		}
		random--;
		if (cardPick == cards.size() || aiChoice == cards.size() || cardPick == cards2.size() || aiChoice == cards2.size())
		{
			break;
		}
	}
	
	conditionals(cardPick, aiChoice, bet, measurement, gold, round, cards, cards2);

	return gold;
}

// final result
void finalResult(int& gold, int& measure)
{
	if (gold >= 20000000 && measure > 0)
	{
		cout << "KAIJI HAS WON THE OVERALL GAME OF EMPEROR CARD " << endl;
		cout << "Gold :" << gold << endl;
		cout << "Milimeters left before ear destruction: " << measure << endl;
		system("pause");
		system("cls");
		cout << "I heard you were good at gambling Kaiji, I didnt expect you to be this good " << endl;
		system("pause");
		system("cls");
		cout << "Kneel and apologize to those youve harmed you old fucking bastard " << endl;
		system("pause");
		system("cls");
	}
	else if (gold < 20000000 && measure > 0)
	{
		cout << "KAIJI HAS WON THE OVERALL GAME OF EMPEROR CARD " << endl;
		cout << "Gold :" << gold << endl;
		cout << "Milimeters left before ear destruction: " << measure << endl;
		system("pause");
		system("cls");
		cout << R"(Kaiji thinks to himself* 
	had I had better decision making I wouldve won much more gold, no matter at least my ear is alright)" << endl;
		system("pause");
		system("cls");
	}
	else if (measure < 0)
	{
		cout << "KAIJI HAS LOST THE OVERALL GAME OF EMPEROR CARD " << endl;
		cout << "Gold :" << gold << endl;
		cout << "Milimeters left before ear destruction: " << measure << endl;
		system("pause");
		system("cls");
		cout << "Whats the matter Kaiji cant hear??? AHAHHAHAHAHHAHAHAHAHAHAH " << endl;
		system("pause");
		system("cls");
		cout << "Kaiji has fainted from the pain " << endl;
		system("pause");
		system("cls");
	}
}

int main()
{

	srand(time(NULL));

	splashScreen();
	
	vector<string>EmperorDeck;
	EmperorDeck.push_back("Civilian");
	EmperorDeck.push_back("Civilian");
	EmperorDeck.push_back("Civilian");
	EmperorDeck.push_back("Civilian");
	EmperorDeck.push_back("Emperor");

	vector<string>SlaveDeck;
	SlaveDeck.push_back("Civilian");
	SlaveDeck.push_back("Civilian");
	SlaveDeck.push_back("Civilian");
	SlaveDeck.push_back("Civilian");
	SlaveDeck.push_back("Slave");


	int rounds = 1;
	int measurement = 30;
	int gold = 0;

	cout << " The game of emperor card will now begin..... " << endl;
	system("pause");
	system("cls");
	cout << "Kaiji will play the Emperor deck first and switch every 3 rounds thereafter..... " << endl;
	system("pause");
	system("cls");

	do {

		cout << " Round " << rounds << "/12" << endl;

		playRound(gold, rounds, measurement, EmperorDeck, SlaveDeck);


		system("pause");
		system("cls");
		rounds++;
	} while (rounds <= 12);

	finalResult(gold, measurement);

	system("pause");
	return 0;
}
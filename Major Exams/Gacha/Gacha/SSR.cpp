#include "SSR.h"
#include"ItemUnit.h"
#include<iostream>

SSR::SSR()
{
	this->rPoints = 50;
	this->rPulls = 1;
}


SSR::~SSR()
{
}

void SSR::apply(PlayerUnit * user)
{
	cout << user->getName() << " SSR " << endl;
	user->getPoints(this->rPoints);
	user->getPulls(this->rPulls);
	user->mSSR += 1;
}

#pragma once
#include"PlayerUnit.h"
#include<string>

using namespace std;


class ItemUnit
{
public:
	ItemUnit();
	~ItemUnit();

	virtual void apply(PlayerUnit* user);


	string rName;
	int mCrystals;
	int rPoints;
	int rDamage;
	int rHealth;
	int rPulls;
};


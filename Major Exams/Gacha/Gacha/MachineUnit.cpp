#include "MachineUnit.h"
#include "PlayerUnit.h"
#include"ItemUnit.h"
#include "Bomb.h"
#include "Crystal.h"
#include "R.h"
#include "SR.h"
#include "SSR.h"
#include "Potion.h"
#include<iostream>

using namespace std;

MachineUnit::MachineUnit()
{
	this->payment = 5;

	mItems.push_back(new R()); // 0
	mItems.push_back(new SR());// 1
	mItems.push_back(new SSR()); // 2
	mItems.push_back(new Potion()); //3
	mItems.push_back(new Bomb()); // 4
	mItems.push_back(new Crystal()); // 5
}


MachineUnit::~MachineUnit()
{
	for (int i = 0; i < mItems.size(); i++)
		delete mItems[i];
	mItems.clear();
}

void MachineUnit::simulation(PlayerUnit * user)
{
	int randSelect = rand() % 100 + 1;

	user->mCrystals = user->mCrystals - payment;

	if (randSelect == 1)//SSR
	{
		mItems[2]->apply(user);
	}
	else if (randSelect > 1 && randSelect < 11)//SR
	{
		mItems[1]->apply(user);
	}
	else if (randSelect > 10 && randSelect < 51)//R
	{
		mItems[0]->apply(user);
	}
	else if (randSelect > 50 && randSelect < 66)//HP
	{
		mItems[3]->apply(user);
	}
	else if (randSelect > 65 && randSelect < 86)//Bomb
	{
		mItems[4]->apply(user);
	}
	else if (randSelect > 85 && randSelect < 101)//Crystal
	{
		mItems[5]->apply(user);
	}
}

#include <iostream>
#include<time.h>
#include<conio.h>
#include<cstdlib>
#include"ItemUnit.h"
#include"MachineUnit.h"
#include"PlayerUnit.h"
#include<vector>
#include<string>

using namespace std;

int main()
{

	PlayerUnit*Unit = new PlayerUnit("Player", 100, 100, 0, 0);
	MachineUnit*MUnit = new MachineUnit;

	for (;;)
	{
		srand(time(NULL));

		Unit->printRoll();

		MUnit->simulation(Unit);

		if (Unit->mHealth <= 0 || Unit->mCrystals <= 0)
			break;

		system("pause");
		system("cls");
	}

	if (Unit->mRarityPoints >= 100)
	{
		cout << "I didnt know winning was an option" << endl;
		Unit->printLastRoll();
		system("pause");
	}

	else if (Unit->mHealth <= 0)
	{
		cout << "Imagine Dying skskskks " << endl;
		Unit->printLastRoll();
		system("pause");
	}
	else if (Unit->mCrystals <= 0)
	{
		cout << "No more crystals yikes" << endl;
		Unit->printLastRoll();
		system("pause");
	}
	system("pause");
	return 0;
}
#include "R.h"
#include<iostream>


R::R()
{
	this->rPoints = 1;
	this->rPulls = 1;
}


R::~R()
{
}

void R::apply(PlayerUnit * user)
{
	cout << user->getName() << " R " << endl;
	user->getPoints(this->rPoints);
	user->getPulls(this->rPulls);
	user->mR += 1;
}

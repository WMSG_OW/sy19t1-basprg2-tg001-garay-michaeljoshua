#include "SR.h"
#include<iostream>


SR::SR()
{
	this->rPoints = 10;
	this->rPulls = 1;
}


SR::~SR()
{
}

void SR::apply(PlayerUnit * user)
{
	cout << user->getName() << " SR " << endl;
	user->getPoints(this->rPoints);
	user->getPulls(this->rPulls);
	user->mSR += 1;
}

#pragma once
#include<string>

using namespace std;

class ItemUnit;
class PlayerUnit
{

public:
	PlayerUnit(string name, int hp, int crystals, int points, int pulls);
	~PlayerUnit();

	void ApplyDamage(int value);
	void getPoints(int value);
	void getPulls(int value);
	void heal(int value);
	void crystal(int value);

	string getName();

	void printRoll();
	void printLastRoll();

	string mName;
	int mHealth;
	int mCrystals;
	int mRarityPoints;
	int mPulls;
	int mSSR = 0;
	int mSR = 0;
	int mR = 0;
	int mCrystalspull = 0;
	int mHPotions = 0;
	int mBomb = 0;
};


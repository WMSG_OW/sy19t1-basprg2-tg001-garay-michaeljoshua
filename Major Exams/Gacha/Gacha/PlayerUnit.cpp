#include "PlayerUnit.h"
#include<iostream>

using namespace std;

PlayerUnit::PlayerUnit(string name, int hp, int crystals, int points, int pulls)
{
	mName = name;
	mHealth = hp;
	mCrystals = crystals;
	mRarityPoints = points;
	mPulls = pulls;
}

PlayerUnit::~PlayerUnit()
{
}

void PlayerUnit::ApplyDamage(int value)
{
	if (value > 0)
	{
		mHealth -= value;
	}
}

void PlayerUnit::getPoints(int value)
{
	if (value > 0)
	{
		mRarityPoints += value;
	}
}

void PlayerUnit::getPulls(int value)
{
	if (value > 0)
	{
		mPulls += value;
	}
}

void PlayerUnit::heal(int value)
{
	if (value > 0)
	{
		mHealth += value;
	}
}

void PlayerUnit::crystal(int value)
{
	if (value > 0)
	{
		mCrystalspull += value;
	}
}

string PlayerUnit::getName()
{
	return this->mName;
}

void PlayerUnit::printRoll()
{
	cout << "NAME: " << mName << endl;
	cout << "HP: " << mHealth << endl;
	cout << "CRYSTALS: " << mCrystals << endl;
	cout << "POINTS: " << mRarityPoints << endl;
	cout << "PULLS: " << mPulls << endl;
}

void PlayerUnit::printLastRoll()
{
	cout << "HP: " << mHealth << endl;
	cout << "CRYSTALS: " << mCrystals << endl;
	cout << "POINTS: " << mRarityPoints << endl;
	cout << "PULLS: " << mPulls << endl;
	cout << "SSR's: " << mSSR << endl;
	cout << "SR's: " << mSR << endl;
	cout << "R's: " << mR << endl;
	cout << "BOMBS: " << mBomb << endl;
	cout << "HP POTIONS: " << mHPotions << endl;
	cout << "CRYSTALS PULLED: " << mCrystalspull << endl;
}



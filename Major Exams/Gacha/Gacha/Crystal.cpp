#include "Crystal.h"
#include <iostream>


Crystal::Crystal()
{
	this->mCrystals = 15;
	this->rPulls = 1;
}


Crystal::~Crystal()
{
}

void Crystal::apply(PlayerUnit * user)
{
	cout << user->getName() << " + 15 Crystals" << endl;
	user->crystal(this->mCrystals);
	user->getPulls(this->rPulls);
	user->mCrystalspull += 15;
}

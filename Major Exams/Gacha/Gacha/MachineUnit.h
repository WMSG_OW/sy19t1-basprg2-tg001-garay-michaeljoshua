#pragma once
#include<string>
#include <iostream>
#include <vector>

using namespace std;
class PlayerUnit;
class ItemUnit;
class MachineUnit
{
public:
	MachineUnit();
	~MachineUnit();

	int payment;

	void simulation(PlayerUnit* user);

private:
	vector<ItemUnit*> mItems;
};


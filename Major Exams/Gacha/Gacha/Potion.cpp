#include "Potion.h"
#include<iostream>

Potion::Potion()
{
	this->rHealth = 30;
	this->rPulls = 1;
}


Potion::~Potion()
{

}

void Potion::apply(PlayerUnit * user)
{
	cout << user->getName() << " 30 heals" << endl;
	user->heal(this->rHealth);
	user->getPulls(this->rPulls);
	user->mHPotions += 1;
}

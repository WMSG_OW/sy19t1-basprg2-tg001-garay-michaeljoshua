#include <iostream>
#include <time.h>

using namespace std;


// ask player for his bet (using reference)

int bet(int& gold, int& gamble)
{

	gold = gold - gamble;

	return gold; 
}


// dice roll (applicable to both ai an player

int diceRoll(int& roll)
{
	int die1 = rand() % 6;
	int die2 = rand() % 6;

	roll = die1 + die2;

	return roll;
}

// payout 

int payout(int& gold, int playerRoll, int aiRoll, int bet)
{

	if (aiRoll == 2)
	{
		aiRoll = 13;
	}
	if (playerRoll == 2)
	{
		playerRoll = 13;
	}

	if (playerRoll > aiRoll)
	{
		if (playerRoll == 13)
		{
			cout << "Snake Eyesssss" << endl;
			gold = gold + (bet * 3);
		}
		else 
		{
			gold = gold + (bet * 2);
		}
		
	}
	else if (aiRoll > playerRoll)
	{
		if (aiRoll == 13)
		{
			cout << "Snake Eyesssss" << endl;
			gold = gold - (bet * 3);
		}
		else 
		{
			gold = gold;
		}
		
	}
	else if (aiRoll == playerRoll)
	{
		cout << "Draw" << endl;
	}
	return  gold;
}

// play round
void playRound()
{
	int counter = 1;
	int money = 1000;
	for (;;)
	{
		int input;
		cout << "Round : " << counter << endl;
		cout << endl;

		cout << "How much do you want to bet??" << endl;
		cin >> input;

		if (input < 0 || input > money)
		{
			for (;;)
			{
				cout << "Please input a valid value " << endl;
				cin >> input;
				if (input > 0 && input < money)
				{
					break;
				}
			}
		}

		bet(money, input);

		int diceSum = 0;
		cout << "Current gold : " << money << endl;

		cout << endl << endl;

		cout << "Player is rolling dice " << endl;
		int playerRoll = diceRoll(diceSum);
		cout << "Player Roll is = " << playerRoll << endl;

		cout << "Ai is rolling dice " << endl;
		int aiRoll = diceRoll(diceSum);
		cout << "Ai Roll is = " << aiRoll << endl;

		payout(money, playerRoll, aiRoll, input);

		cout << "Remaining gold : " << money << endl;

		system("pause");
		system("cls");

		counter++;

		if (money <= 0)
		{
			break;
		}
	}
}

int main()
{
	srand(time(NULL));
	// will continously call until the player no longer has any gold
	playRound();

	system("pause");
	return 0;
}
#include<iostream>
#include<time.h>
#include<string>

using namespace std;

// items class
struct Loot
{
	string name;
	int value;
};


// generates random item
Loot *randomPackage()
{
	Loot *Items = new Loot[5];
	Items[0].name = "Mithril Ore";
	Items[0].value = 100;
	Items[1].name = "Sharp Talon";
	Items[1].value = 50;
	Items[2].name = "Thick Leather";
	Items[2].value = 25;
	Items[3].name = "Jellopy";
	Items[3].value = 5;
	Items[4].name = "CursedStone";
	Items[4].value = 0;
	
	int x = rand() % 5;
	cout << "You found a " << Items[x].name << " with value " << Items[x].value << endl;

	return &Items[x];
}

//deducts fee, continous looting, gold not immediately added until exit, if looted everything else thats not cursed stone ask to continue looting
int enterDungeon(int& gold, int fee, int& tempGold)
{

	Loot* Items = new Loot;

	cout << "Greeting traveller, I see you want to enter the dungeon, thatll cost you 25 gold" << endl;
	system("pause");
	system("cls");

	gold -= fee;

	char choice;
	int multiplier = 1;

	system("pause");
	system("cls");

	//main program loop
	for (;;)
	{
		Items = randomPackage();
		// adds up to temp gold with multipliers 
		tempGold = tempGold + (Items->value * multiplier);
		if (Items->value == 0) //checks for cursed stone if cursed stone terminates program
		{
			cout << "You have encountered a " << Items->name << " and have fainted " << endl;
			system("pause");
			system("cls");
			break;
		}
		else if (Items->value != 0) // continue exploring
		{
			cout << "Would you like to continue exploring??  y/n ";
			cin >> choice;
			if (choice == 'y')
			{
			    if (multiplier < 4)
				{
					multiplier++;
				}
			}
			else if (choice == 'n') // end program manually
			{
				cout << "Thank you for coming " << endl;
				system("pause");
				gold = gold + tempGold;
				break;
			}
			system("pause");
			system("cls");
		}
	} 
	// determining win lose via gold 
	if (gold < 500) 
	{
		cout << "Current Gold: " << gold << endl;
		cout << "Defeat" << endl;
	}
	else if (gold >= 500)
	{
		cout << "Current Gold: " << gold << endl;
		cout << "Victory" << endl;
	}
	return gold;
}


int main()
{
	srand(time(NULL));

	int startingGold = 50;
	int tempMoney = 0;

	enterDungeon(startingGold, 25, tempMoney);

	system("pause");
	return 0;
}
#pragma once
#include"Project4/Header.h"

class Circle : public Shape
{
public:
	Circle();
	~Circle();

	int getRadius();
	void setRadius(int measure);
	int getArea() override;

private:
	int mRadius;
};


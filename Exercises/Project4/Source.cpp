#include<iostream>
#include<string>
#include<vector>
#include"Project4/Header.h"
#include"Project4/Square.h"
#include"Project4/Circle.h"
#include"Project4/Triangle.h"

using namespace std;

void displayShape(Shape *shape)
{
	cout << " Shape type " << shape->getName() << endl;
	cout << " # of sides " << shape->getNumSides() << endl; 
	cout << " Area " << shape->getArea() << endl;
}

void main()
{
	vector<Shape*>Shapes;
	

	Square *square = new Square;
	square->setName("Square");
	square->setNumSides(4);
	square->setLength(5);

	displayShape(square);

	Circle *circle = new Circle;
	circle->setName("Circle");
	circle->setNumSides(0);
	circle->setRadius(3);

	displayShape(circle);

	Triangle *triangle = new Triangle;
	triangle->setName("Triangle");
	triangle->setNumSides(3);
	triangle->setLengthSide1(5);
	triangle->setLengthSide2(3);
	triangle->setLengthSide3(6);

	displayShape(triangle);

	system("pause");
	system("cls");

	Shapes.push_back(square);
	Shapes.push_back(circle);
	Shapes.push_back(triangle);

	for (int i = 0; i < Shapes.size(); i++)
	{
		Shape *Shape = Shapes[i];

		cout << Shape->getName() << " \tSides " << Shape->getNumSides() << " \tArea " << Shape->getArea() << endl;
	}

	system("pause");

}
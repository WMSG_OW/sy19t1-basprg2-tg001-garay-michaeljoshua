#include<iostream>
#include<string>
#include<vector>
#include"Header.h"
#include"Square.h"
#include"Circle.h"

using namespace std;

void displayShape(Shape *shape)
{
	cout << " Shape type " << shape->getName() << endl;
	cout << " # of sides " << shape->getNumSides() << endl; 
	cout << " Area " << shape->getArea() << endl;
}

void main()
{
	Square *square = new Square;
	square->setName("Square");
	square->setNumSides(4);
	square->setLength(5);

	displayShape(square);

	Circle *circle = new Circle;
	circle->setName("Circle");
	circle->setNumSides(0);
	circle->setRadius(3);

	displayShape(circle);


	system("pause");

}
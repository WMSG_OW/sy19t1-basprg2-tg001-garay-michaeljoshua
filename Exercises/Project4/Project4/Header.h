#pragma once
#include<string>


using namespace std; 

class Shape
{

public:
	Shape();
	~Shape();

	string getName();
	int getNumSides();

	void setName(string name);
	void setNumSides(int sides);


	virtual int getArea();

private: 
	string mName;
	int mNumSides;
};
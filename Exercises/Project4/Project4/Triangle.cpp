#include "Triangle.h"

Triangle::Triangle()
{
}

Triangle::~Triangle()
{
}

int Triangle::getLengths()
{
	return mLengthSide1;
	return mLengthSide2;
	return mLengthSide3;
}

void Triangle::setLengthSide1(int measure)
{
	mLengthSide1 = measure;
}

void Triangle::setLengthSide2(int measure)
{
	mLengthSide2 = measure;
}

void Triangle::setLengthSide3(int measure)
{
	mLengthSide3 = measure;
}

int Triangle::getArea()
{
	return (mLengthSide3*mLengthSide2)/2;
}

#pragma once
#include"Header.h"

class Square : public Shape
{
public:
	Square();
	~Square();

	int getLength();
	void setLength(int measure);
	int getArea() override;

private:
	int mLengthSides;
};


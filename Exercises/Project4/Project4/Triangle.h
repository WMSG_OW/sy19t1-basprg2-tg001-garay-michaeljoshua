#pragma once
#include"Header.h"

class Triangle : public Shape
{
public:
	Triangle();
	~Triangle();

	int getLengths();
	void setLengthSide1(int measure);
	void setLengthSide2(int measure);
	void setLengthSide3(int measure);
	int getArea() override;

private:
	int mLengthSide1;
	int mLengthSide2;
	int mLengthSide3;
};
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

int factorial(int number)
{
	int factorial = 1;

	cout << "You have chosen " << number << endl;
	for (int i = 1; i <= number; i++)
	{
		factorial = factorial * i;
	}
	cout << "Factorial of  " << number << " = " << factorial << endl;
	return number;
}


int main()
{
	int numberOutput; 
	cout << "EX 1-1" << endl;
	cout << "Choose a number you want to factorial" << endl;
	cin >> numberOutput;
	factorial(numberOutput);
	system("pause");
	system("cls");
	return 0;
}
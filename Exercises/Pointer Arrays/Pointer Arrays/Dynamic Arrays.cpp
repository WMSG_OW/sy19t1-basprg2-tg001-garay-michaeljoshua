#include <iostream>
#include <time.h>

using namespace std;


// Create an array using pointer function should fill with random values 
void arrPtr(int* pointerToArray, int size)
{
	for (int i = 0; i < size; i++)
	{
		pointerToArray[i] = rand() % 100;
	}
	for (int i = 0; i < size; i++)
	{
		cout << " array values : " << pointerToArray[i] << endl;
	}
	
}

// Create an array using pointer function should fill with random values  return created array 
int retArr(int* pointerToArray, int size)
{
	for (int i = 0; i < size; i++)
	{
		pointerToArray[i] = rand() % 100;
	}
	for (int i = 0; i < size; i++)
	{
		cout << " array values : " << pointerToArray[i] << endl;
	}
	return *pointerToArray;
}

// Create an array using pointer function should fill with dynamically allocated values return created array delete all elements inside the array after function call
int deleteArrayElements(int* pointerToArray, int size)
{
	int input;
	cout << "Input values inside the array " << endl;
	for (int i = 0; i < size; i++)
	{
		cin >> input;
		pointerToArray[i] = input;
	}
	for (int i = 0; i < size; i++)
	{
		cout << " array values : " << pointerToArray[i] << endl;
	}
	return *pointerToArray;
}


// main

int main()
{
	srand(time(NULL));
	int numArrayFirst [5];
	int numArraySecond[10];
	int *numArrayThird = new int [6];

	arrPtr(numArrayFirst, 5);
	system("pause");
	system("cls");
	retArr(numArraySecond, 10);
	system("pause");
	system("cls");
	deleteArrayElements(numArrayThird, 6);
	delete[] numArrayThird;
	system("pause");
	return 0;
}
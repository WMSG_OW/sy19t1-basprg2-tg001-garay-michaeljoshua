#pragma once
#include"UnorderedArray.h"
#include<cstdlib>
#include<iostream>
#include<string>

using namespace std;

template<class T>
class Queue
{
public:
	Queue(int size)
	{
		mContainer = new UnorderedArray<T>(size);
	}

	void push(T value)
	{
		mContainer->push(value);
	}

	void pop()
	{
		mContainer->remove(0);
		mContainer->pop();
	}

	void display()
	{
		for (int i = 0; i < (*mContainer).getSize(); i++) 
		{
			cout << (*mContainer)[i] << "\n";
		}
	}
	
	void removeAll()
	{
		for (int i = 0; i < (*mContainer).getSize(); i++)
		{
			mContainer->remove(i);
			mContainer->pop();
		}
	}

    T top()
	{
		if ((*mContainer).getSize() != NULL)
			return (*mContainer)[0];
		else return 0;
	}
private:
	UnorderedArray<T>*mContainer;
};


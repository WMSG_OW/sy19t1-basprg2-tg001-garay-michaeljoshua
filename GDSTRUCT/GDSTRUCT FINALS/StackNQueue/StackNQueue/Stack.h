#pragma once
#include"UnorderedArray.h"
#include<cstdlib>
#include<iostream>
#include<string>

using namespace std;

template<class T>
class Stack
{
public:
	Stack(int size)
	{
		mContainer = new UnorderedArray<T>(size);
	}
	void push(T value)
	{
		mContainer->push(value);
	}

	void pop()
	{
		mContainer->remove((*mContainer).getSize() - 1);
		mContainer->pop();
	}

	void display()
	{
		for (int i = (*mContainer).getSize(); i > 0; i--)
		{
			cout << (*mContainer)[i - 1] << "\n";
		}
	}
	
	void removeAll()
	{
		for (int i = (*mContainer).getSize(); i > -1; i--)
		{
			mContainer->remove(i - 1);
			mContainer->pop();
		}
	}

	T top()
	{
		if ((*mContainer).getSize() != NULL)
			return (*mContainer)[(*mContainer).getSize() - 1];
		else return 0;
	}

private:
	UnorderedArray<T>*mContainer;
};


#include<iostream>
#include"Queue.h"
#include"Stack.h"
#include<cstdlib>

using namespace std;

int main()
{
	int choice;
	int size;

	cout << "Enter starting size \n";
	cin >> size;
	Queue<int>Queue(size);
    Stack<int>Stack(size);
	do {
		cout << "What do you want to do?" << endl;
		cout << "1 - Push \n";
		cout << "2 - Pop \n";
		cout << "3 - Print everything then empty set\n";
		cout << "4 - Exit \n";
		cin >> choice;
		switch (choice)
		{
		case 1:
			int pushNum;
			cout << " Enter number to add \n";
			cin >> pushNum;
			Queue.push(pushNum);
			Stack.push(pushNum);
			system("pause");
			break;
		case 2:
			cout << "Popping Top Element...... \n";
			Queue.pop();
			Stack.pop();
			system("pause");
			break;
		case 3:
			cout << "Displaying all Elements.... \n";
			cout << " Queue : \n";
			Queue.display();
			cout << " Stack : \n";
			Stack.display();

			Queue.removeAll();
			Stack.removeAll();

			system("pause");
			break;
		case 4:
			cout << "Exiting Program..... \n";
			system("pause");
			break;
		}
		cout << " Top of Element Sets: " << endl;
		cout << "Queue : " << Queue.top() << endl;
		cout << "Stack : " << Stack.top() << endl;
		system("pause");
		system("cls");
	} while (choice != 4);

	system("pause");
	return 0;
}
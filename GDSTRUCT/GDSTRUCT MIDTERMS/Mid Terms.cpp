#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>
#include <string>

using namespace std;

void main()
{
	srand(time(NULL));
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	for (;;)
	{
		cout << "\nGenerated array: " << endl;
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";

		cout << "\n\nWhat do you want to do? : " << endl;
		string activities[3]{ " Remove Element at Index ", " Search For Element ", " Expand and Generate Random Values " };
		for (int i = 0; i < 3; i++)
		{
			cout << " [" << i << "] " << activities[i] << endl;
		}
		int input;
		cin >> input;
		cout << endl;

		if (input == 0)
		{
			cout << "Remove an element ";
			cin >> input;

			unordered.remove(input);
			ordered.remove(input);
		}
		else if (input == 1)
		{
			int result;
			cout << "Search for element....";
			cin >> input;
			cout << "Unordered Array(Linear Search):\n";
		     result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary\Search):\n";
			 result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
		}
		else if (input == 2)
		{
			int add;
			cout << "How many elements would you like to add? " << endl;
			cin >> add;

			for (int i = 0; i < add; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
		}

		system("pause");
		system("cls");
	}

	//cout << "Ordered Array(Binary Search):\n";
	//result = ordered.binarySearch(input);
	//if (result >= 0)
	//	cout << input << " was found at index " << result << ".\n";
	//else
	//	cout << input << " not found." << endl;
	system("pause");
}
#include<iostream>

using namespace std;

int printSum(int num)
{
	if (num == 0)
		return 0;

	else
		return(num % 10 + printSum(num / 10));
}

void printFibonacci(int number, int n1, int n2)
{
	int n3;
	if (number > 0)
	{
		n3 = n1 + n2;
		n1 = n2;
		n2 = n3;
		cout << n3 << " ";
		printFibonacci(number - 1, n1, n2);
	}
}

bool isPrime(int n, int i = 2)
{
	if (n <= 2)
		return (n == 2) ? true : false;
	if (n % i == 0)
		return false;
	if (i * i > n)
		return true;

	return isPrime(n, i + 1);
	

	
}

int primeSearch(int number)
{
	if (isPrime(number))
		cout << "Yes";
	else
		cout << "No";

	return number;
}

void main()
{
	int input;

	cout << "EX1 : " << endl;

	cout << "Enter a number ";
	cin >> input;

	cout << "Sum = " << printSum(input);

	system("pause");
	system("cls");

	cout << "EX 2 : " << endl;
	int n;

	cout << "Enter fibonacci number ";
	cin >> n;
	int n1 = 0;
	int n2 = 1;
	cout << "Fibonacci Sequence : 0 1 ";
	printFibonacci(n - 2, n1, n2);
	system("pause");
	system("cls");

	cout << "EX 3 : " << endl;
	int num;
	cout << "Enter a number for Prime search ";
	cin >> num;
	primeSearch(num);

	system("pause");

}